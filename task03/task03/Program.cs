﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task03
{
    class Program
    {
        static void Main(string[] args)
        {
            calculate(5);
        }

        static void calculate(int a)
        {
            Console.WriteLine($"{a} + {a} = {a + a}");
            Console.WriteLine($"{a} - {a} = {a - a}");
            Console.WriteLine($"{a} * {a} = {a * a}");
            Console.WriteLine($"{a} / {a} = {a / a}");
        }
    }
}
